package bsmch.CloudsOfSteelPIT.services;

import java.util.List;
import org.springframework.stereotype.Service;

import org.springframework.beans.factory.annotation.Autowired;
import bsmch.CloudsOfSteelPIT.repositories.InterceptorRepository;
import bsmch.CloudsOfSteelPIT.models.InterceptorModel;


@Service
public class InterceptorService {
    @Autowired
    InterceptorRepository interceptorRepository;

    public List<InterceptorModel> getAllInterceptors() {
        return interceptorRepository.findAll();
    }

    public InterceptorModel addInterceptor(InterceptorModel interceptor) {
        return interceptorRepository.save(interceptor);
    }

    public InterceptorModel getInterceptorById(int id){
        return interceptorRepository.findById(id).get();
    }

    public void UpdateInterceptor(InterceptorModel interceptor) {
        interceptorRepository.save(interceptor);
    }
    public void deleteInterceptorById(int id) {
        interceptorRepository.deleteById(id);
    }
}

