package bsmch.CloudsOfSteelPIT.services;

import bsmch.CloudsOfSteelPIT.models.InterceptorModel;
import bsmch.CloudsOfSteelPIT.models.SupplyModel;
import bsmch.CloudsOfSteelPIT.repositories.SupplyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SupplyService {

    @Autowired
    private SupplyRepository supplyRepository;

    @Autowired
    InterceptorService interceptorService;

    public void addSupply(SupplyModel supply){
        supplyRepository.save(supply);
    }

    public void updateSupply(SupplyModel supply) {
        supplyRepository.save(supply);
    }

    public List<SupplyModel> getAllSupplies() { return supplyRepository.findAll(); }

    public SupplyModel getSupplyById(int id){
        return supplyRepository.findById(id).get();
    }

    public SupplyModel getSupplyByInterceptorID(int id){
        InterceptorModel interceptor = interceptorService.getInterceptorById(id);
        Optional<SupplyModel> supplyModel = supplyRepository.findByInterceptorModel(interceptor);
        return supplyModel.orElse(null);
    }

    public void deleteSupplyById(int id) {
        supplyRepository.deleteById(id);
    }
}
