package bsmch.CloudsOfSteelPIT.services;

import bsmch.CloudsOfSteelPIT.models.*;
import bsmch.CloudsOfSteelPIT.repositories.LaunchRepository;

import bsmch.CloudsOfSteelPIT.Utillities.Calc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.*;

@Service
public class LaunchService {
    @Autowired
    LaunchRepository launchRepository;

//    @Autowired
//    InterceptorService interceptorService;

    @Autowired
    SupplyService supplyService;

    public List<LaunchModel> getAllLaunches() {
        return launchRepository.findAll();
    }

    public List<LaunchModel> getLaunchesAfterDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, -2);
        Date date = calendar.getTime();
        return launchRepository.getAllByLaunchDateAfter(LocalDateTime.now().minusHours(48));
    }

    public void addLaunch(launchData launchData) {
        LaunchModel launchModel = convertLaunchData(launchData);
        launchRepository.save(launchModel);
    }

    public LaunchModel convertLaunchData(launchData launchData){
        Vector3 crashLocation = Calc.calcCrashLocation(launchData);

        List<SupplyModel> supplies = supplyService.getAllSupplies();
        TreeMap<Double, SupplyModel> distances = new TreeMap<>();

        InterceptorModel launchInterceptor = null;
        boolean intercepted = false;
        String reason = "";

        supplies.forEach(supply -> {
            InterceptorModel interceptor = supply.getInterceptor();
            Vector3 interceptorLocation = new Vector3(interceptor.x, interceptor.y, interceptor.z);
            Double distance = interceptorLocation.calcDistance(crashLocation);
            distances.put(distance,supply);
        });

        InterceptorModel firstInterceptor = distances.firstEntry().getValue().getInterceptor();

        for (Double distance : distances.keySet()) {
            SupplyModel supply = distances.get(distance);
            InterceptorModel interceptor = supply.getInterceptor();

            if(distance <= interceptor.radius){
                if(interceptor.isActive == 1){
                    if(supply.getAmount() >= supply.getMinimum()) {
                        launchInterceptor = interceptor;
                        intercepted = true;
                        reason = "";
                        supply.reduce();
                        supplyService.updateSupply(supply);
                        break;
                    }

                    else{ reason = "סוללה ללא מלאי"; }
                }

                else{ reason = "סוללה מנותקת"; }
            }

            else{ reason = "נחיתה בשטח פתוח"; }
        }

        if( launchInterceptor == null){
            launchInterceptor = firstInterceptor;
        }

        return new LaunchModel(launchData.missile_type, launchInterceptor, intercepted, reason);
    }
}
