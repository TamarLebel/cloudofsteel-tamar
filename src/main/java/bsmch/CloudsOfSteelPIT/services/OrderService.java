package bsmch.CloudsOfSteelPIT.services;

import bsmch.CloudsOfSteelPIT.models.Order;
import bsmch.CloudsOfSteelPIT.repositories.OrderRepository;
import org.aspectj.weaver.ast.Or;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderService {
    @Autowired
    private OrderRepository orderRepository;
    public void addOrder(Order order){
        this.orderRepository.save(order);
    }

    public List<Order> getAll(){ return this.orderRepository.findAll(); }

    public void updateOrder(Order order) { this.orderRepository.save(order); }

    public List<Order> getAllNotApproved() { return this.orderRepository.getAllByIsApproved(0); }

    public void deleteOrderById(int id) { this.orderRepository.deleteById(id); }
}
