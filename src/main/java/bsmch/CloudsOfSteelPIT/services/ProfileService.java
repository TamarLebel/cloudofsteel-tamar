package bsmch.CloudsOfSteelPIT.services;

import bsmch.CloudsOfSteelPIT.models.Profile;
import bsmch.CloudsOfSteelPIT.models.User;
import bsmch.CloudsOfSteelPIT.repositories.ProfileRepository;
import bsmch.CloudsOfSteelPIT.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProfileService {
    @Autowired
    private ProfileRepository profileRepository;
    @Autowired
    private UserRepository userRepository;
    public Profile getProfileByUser(int userId)
    {
        User user=userRepository.findById(userId).get();
        return profileRepository.getProfileByUserId(user);
    }

    public void updateProfile(Profile profile){
        profileRepository.save(profile);
    }
}
