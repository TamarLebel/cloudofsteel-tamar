package bsmch.CloudsOfSteelPIT.services;

import bsmch.CloudsOfSteelPIT.models.PermissionType;
import bsmch.CloudsOfSteelPIT.repositories.PermissionTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class PermissionTypeService {
    @Autowired
    private PermissionTypeRepository permissionTypeRepository;
    public List<PermissionType> getAll(){return  permissionTypeRepository.findAll();}
}
