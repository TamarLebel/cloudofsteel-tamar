package bsmch.CloudsOfSteelPIT.services;

import bsmch.CloudsOfSteelPIT.models.PermissionType;
import bsmch.CloudsOfSteelPIT.models.User;
import bsmch.CloudsOfSteelPIT.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;
    public List<User> getAll(){return userRepository.findAll();}
    public User getUserById(int id){
        return userRepository.findById(id).orElse(null);
    }
    public User getUserByNameAndPassword(String name, String password){return userRepository.getUserByUsernameAndPassword(name, password);}
    public PermissionType getPermissionTypeByName(String name){
        return userRepository.getUserByUsername(name).getPermissionType();
    }
    public User getUserByName(String  name){
        return userRepository.getUserByUsername(name);
    }

    public void updateUser(User user){
        userRepository.save(user);
    }
    public boolean addUser(User user){
        if(getUserByName(user.getName())==null) {
            userRepository.save(user);
            return true;
        }
        return false;
    }
}
