package bsmch.CloudsOfSteelPIT.Utillities;

import bsmch.CloudsOfSteelPIT.models.*;

public class Calc {
	final static double CONST_G = 0.45;
	public static Vector3 calcCrashLocation(launchData movement) {
		Vector3 curr_pos = movement.getOrigin();
		double height = 0;
		double timeLeft = (Math.sqrt(Math.pow(movement.getVy(), 2) + 2* CONST_G *(curr_pos.getY() - height)) + movement.getVy())/CONST_G;
		Vector3 pos = new Vector3();
		pos.setX(movement.getOrigin().getX() + timeLeft * movement.getVx());
		pos.setY(movement.getOrigin().getY() + timeLeft * movement.getVy() - (CONST_G/2) * Math.pow(timeLeft,2));
		pos.setZ(movement.getOrigin().getZ() + timeLeft * movement.getVz());
		return (pos);
	}
}
