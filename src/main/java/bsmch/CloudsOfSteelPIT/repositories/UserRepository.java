package bsmch.CloudsOfSteelPIT.repositories;

import bsmch.CloudsOfSteelPIT.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User,Integer> {
    User getUserByUsernameAndPassword(String username,String password);
    User getUserByUsername(String username);
}
