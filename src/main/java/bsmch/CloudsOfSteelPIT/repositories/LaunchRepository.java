package bsmch.CloudsOfSteelPIT.repositories;

import bsmch.CloudsOfSteelPIT.models.LaunchModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Repository
public interface LaunchRepository extends JpaRepository<LaunchModel, Integer> {
    List<LaunchModel> getAllByLaunchDateAfter(LocalDateTime launchDate);

}
