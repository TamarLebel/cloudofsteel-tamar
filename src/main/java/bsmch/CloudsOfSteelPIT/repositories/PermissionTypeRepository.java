package bsmch.CloudsOfSteelPIT.repositories;

import bsmch.CloudsOfSteelPIT.models.PermissionType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PermissionTypeRepository extends JpaRepository<PermissionType,Integer> {
}
