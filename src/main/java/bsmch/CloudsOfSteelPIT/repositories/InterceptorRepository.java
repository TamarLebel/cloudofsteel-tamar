package bsmch.CloudsOfSteelPIT.repositories;

import bsmch.CloudsOfSteelPIT.models.InterceptorModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InterceptorRepository extends JpaRepository<InterceptorModel, Integer> {
    InterceptorModel getById(int id);
}
