package bsmch.CloudsOfSteelPIT.repositories;

import bsmch.CloudsOfSteelPIT.models.Profile;
import bsmch.CloudsOfSteelPIT.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProfileRepository extends JpaRepository<Profile,Integer> {
Profile getProfileByUserId(User user);
}
