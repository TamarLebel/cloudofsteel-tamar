package bsmch.CloudsOfSteelPIT.repositories;

import bsmch.CloudsOfSteelPIT.models.InterceptorModel;
import bsmch.CloudsOfSteelPIT.models.SupplyModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SupplyRepository extends JpaRepository<SupplyModel, Integer> {
        Optional<SupplyModel> findByInterceptorModel(InterceptorModel interceptor);
}
