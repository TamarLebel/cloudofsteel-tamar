package bsmch.CloudsOfSteelPIT.models;

public class Vector3 {
	private double x;
	private double y;
	private double z;
	public Vector3(){
		this.setX(0);
		this.setY(0);
		this.setZ(0);
	}
	public Vector3(double x,double y,double z){
		this.setX(x);
		this.setY(y);
		this.setZ(z);
	}
	public double getX() {
		return x;
	}
	public void setX(double x) {
		this.x = x;
	}
	public double getY() {
		return y;
	}
	public void setY(double y) {
		this.y = y;
	}
	public double getZ() {
		return z;
	}
	public void setZ(double z) {
		this.z = z;
	}

	@Override
	public String toString() {
		return "{ x=" + x +
				", y=" + y +
				", z=" + z +" }";
	}

	public Double calcDistance(Vector3 secPosition) {
		return Math.sqrt(
				Math.pow(this.getX() - secPosition.getX(),2) +
				Math.pow(this.getY() - secPosition.getY(),2) +
				Math.pow(this.getZ() - secPosition.getZ(),2)
		);
	}
}
