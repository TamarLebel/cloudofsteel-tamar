package bsmch.CloudsOfSteelPIT.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

@Entity
@Table(name = "T_ORDERS")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty
    @Column(name = "ORDER_ID")
    private int id;

    @JsonProperty
    @Column(name = "AMOUNT")
    private int amount;

    @JsonProperty
    @Column(name = "PRICE")
    private int price;

    @JsonProperty
    @Column(name = "IS_APROVED")
    private int isApproved;

    @ManyToOne
    @JoinColumn(name="USER_ID")
    @JsonProperty
    private User userId;

    @ManyToOne
    @JoinColumn(name="INTERCEPTOR_ID")
    @JsonProperty
    private InterceptorModel interceptor;

    public void setIsApproved(int permition) {
        this.isApproved = permition;
    }
}
