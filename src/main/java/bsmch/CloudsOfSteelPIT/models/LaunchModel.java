package bsmch.CloudsOfSteelPIT.models;

import com.fasterxml.jackson.annotation.*;
import org.hibernate.annotations.GenericGenerator;

import java.time.LocalDateTime;
import javax.persistence.*;

@Entity
@Table(name="T_LAUNCHES")
public class LaunchModel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator="seq_launch")
    @GenericGenerator(name = "seq_launch", strategy="increment")
    @Column(name="LAUNCH_ID")
    @JsonProperty
    private int id;

    @Column(name = "ROCKET_TYPE")
    @JsonProperty
    private String rocketType;

    @Column(name="LAUNCH_DATE")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonProperty
    private LocalDateTime launchDate;

    @ManyToOne
    @JoinColumn(name="INTERCEPTOR_ID")
    @JsonProperty
    private InterceptorModel interceptor;

    @Column(name = "INTERCEPTED")
    @JsonProperty
    private int intercepted;

    @Column(name = "FAILED_INTERCEPTED_REASON")
    @JsonProperty
    private String failedReason;

    public LaunchModel(){ }

    public LaunchModel(String rocketType, InterceptorModel interceptor, boolean intercepted, String failedReason){
        this.rocketType = rocketType;
        this.launchDate =  LocalDateTime.now();
        this.interceptor = interceptor;
        this.intercepted = intercepted? 1:0;
        this.failedReason = failedReason;
    }
}
