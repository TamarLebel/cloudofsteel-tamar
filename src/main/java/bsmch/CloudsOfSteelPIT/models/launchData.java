package bsmch.CloudsOfSteelPIT.models;

import java.util.HashMap;
import java.util.Map;

public class launchData {
    public double ver_angle;
    public double hor_angle;
    public double vy;
    public double vx;
    public double vz;
    public String missile_type;
    public Vector3 origin;

    public launchData(){

    }

    @Override
    public String toString() {
        return "launchData{" +
                "ver_angle=" + ver_angle +
                ", hor_angle=" + hor_angle +
                ", vy=" + vy +
                ", vx=" + vx +
                ", vz=" + vz +
                ", missile_type="+ missile_type +
                ", origin=" + origin +
                '}';
    }

    public double getVer_angle() {
        return ver_angle;
    }

    public double getHor_angle() {
        return hor_angle;
    }

    public double getVy() {
        return vy;
    }

    public void setVy(double vy) {
        this.vy = vy;
    }

    public double getVx() {
        return vx;
    }

    public void setVx(double vx) {
        this.vx = vx;
    }

    public double getVz() {
        return vz;
    }

    public void setVz(double vz) {
        this.vz = vz;
    }
    public String getMissileType() {
        return missile_type;
    }

    public void setMissileType(String missileType) {
        this.missile_type = missileType;
    }

    public Vector3 getOrigin() {
        return origin;
    }
}
