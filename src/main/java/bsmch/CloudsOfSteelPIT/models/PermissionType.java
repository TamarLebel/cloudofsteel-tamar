package bsmch.CloudsOfSteelPIT.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

@Entity
@Table(name = "T_PERMISSIONS_TYPES")
public class PermissionType {
    @Id
    @JsonProperty
    @Column(name = "PERMISSION_TYPE_ID")
    private int id;

    @JsonProperty
    @Column(name = "PERMISSION_NAME")
    private String name;
}
