package bsmch.CloudsOfSteelPIT.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "T_SUPPLY")
public class SupplyModel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator="seq_supply")
    @GenericGenerator(name = "seq_supply", strategy="increment")
    @Column(name = "SUPPLY_ID")
    @JsonProperty
    private int id;

    @Column(name = "AMOUNT")
    @JsonProperty
    private int amount;

    @Column(name = "MINIMUM")
    @JsonProperty
    private int minimum;

    @Column(name = "PRICE")
    @JsonProperty
    private int price;

    @JoinColumn(name = "INTERCEPTOR_ID")
    @JsonProperty
    @ManyToOne
    private InterceptorModel interceptorModel;

    public InterceptorModel getInterceptor() {
        return interceptorModel;
    }

    public int getAmount() {
        return amount;
    }

    public int getMinimum() {
        return minimum;
    }

    public void reduce(){
        this.amount--;
    }
}
