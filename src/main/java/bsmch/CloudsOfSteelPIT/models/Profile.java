package bsmch.CloudsOfSteelPIT.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

@Entity
@Table(name = "T_PROFILES")
public class Profile {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty
    @Column(name = "PROFILE_ID")
    private int id;

    @ManyToOne
    @JoinColumn(name="USER_ID")
    @JsonProperty
    private User userId;

    @JsonProperty
    @Column(name = "FIRST_NAME")
    private String firstname;

    @JsonProperty
    @Column(name = "LAST_NAME")
    private String lastname;

    @JsonProperty
    @Column(name = "AGE")
    private int age;

    @JsonProperty
    @Column(name = "CITY")
    private String city;

    @JsonProperty
    @Column(name = "CELLPHONE")
    private String cellphone;

    @JsonProperty
    @Column(name = "EMAIL")
    private String email;
}
