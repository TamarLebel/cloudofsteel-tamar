package bsmch.CloudsOfSteelPIT.models;

import com.fasterxml.jackson.annotation.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name="T_INTERCEPTORS")
public class InterceptorModel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator="seq_inter")
    @GenericGenerator(name = "seq_inter", strategy="increment")
    @Column(name = "INTERCEPTOR_ID")
    @JsonProperty
    public int id;

    @Column(name = "INTERCEPTOR_NAME")
    @JsonProperty
    public String name;

    @Column(name = "X_POSITION")
    @JsonProperty
    public double x;

    @Column(name = "Y_POSITION")
    @JsonProperty
    public double y;

    @Column(name = "Z_POSITION")
    @JsonProperty
    public int z;

    @Column(name = "DEFENSIVE_RADIUS")
    @JsonProperty
    public int radius;

    @Column(name = "IS_ACTIVE")
    @JsonProperty
    public int isActive;

    public String toString() {
        return "{id: " + this.id + " name: " + this.name + " x: " + this.x + " y: " + this.y + " z: " + this.z + " radius: " + this.radius + " isActive: " + this.isActive+ "}";
    }
}