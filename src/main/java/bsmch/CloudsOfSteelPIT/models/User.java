package bsmch.CloudsOfSteelPIT.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "T_USERS")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator="seq")
    @GenericGenerator(name = "seq", strategy="increment")
    @JsonProperty
    @Column(name = "USER_ID")
    private int id;

    @JsonProperty
    @Column(name = "USER_NAME")
    private String username;

    @JsonProperty
    @Column(name = "PASSWORD")
    private String password;

    @ManyToOne
    @JoinColumn(name = "PERMISSION_TYPE_ID",referencedColumnName = "PERMISSION_TYPE_ID")
    @JsonProperty
    private PermissionType permissionType;
//    @Column(name = "PERMISSION_TYPE_ID")

    public PermissionType getPermissionType(){
        return this.permissionType;
    }
    public int getId(){
        return id;
    }
    public String  getName(){
        return username;
    }
}
