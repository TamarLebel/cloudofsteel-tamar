package bsmch.CloudsOfSteelPIT.controllers;

import bsmch.CloudsOfSteelPIT.models.Profile;
import bsmch.CloudsOfSteelPIT.services.ProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = {"http://localhost:3000"})
@RestController
@RequestMapping("/profile")
public class ProfileController {
    @Autowired
    private ProfileService profileService;
    @GetMapping("/userid={userId}")
    public Profile getProfileByUserId(@PathVariable int userId){
return profileService.getProfileByUser(userId);
    }
    @PutMapping("")
    public void updateProfile(@RequestBody Profile profile){
        profileService.updateProfile(profile);
    }
}
