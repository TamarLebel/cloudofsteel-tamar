package bsmch.CloudsOfSteelPIT.controllers;

import bsmch.CloudsOfSteelPIT.models.SupplyModel;
import bsmch.CloudsOfSteelPIT.services.SupplyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = {"http://localhost:3000"})
@RestController
@RequestMapping("/supply")
public class SupplyController {

    @Autowired
    private SupplyService supplyService;

    @GetMapping("")
    public List<SupplyModel> getAll() {
        return supplyService.getAllSupplies();
    }

    @PostMapping("/addSupply")
    public void addSupplyToDb(@RequestBody SupplyModel supply) {
        this.supplyService.addSupply(supply);
    }

    @GetMapping("/getSupply/{id}")
    public SupplyModel getSupplyByID(@PathVariable int id) { return supplyService.getSupplyById(id); }

    @PutMapping("/updateSupply")
    public void updateSupply(@RequestBody SupplyModel supply) {
        supplyService.updateSupply(supply);
    }

    @DeleteMapping("/deleteSupply/{id}")
    public void deleteSupply(@PathVariable int id) {
        supplyService.deleteSupplyById(id);
    }

    @GetMapping("/getByInterceptorId/{id}")
    public SupplyModel getSupplyByInterceptorId(@PathVariable int id) { return this.supplyService.getSupplyByInterceptorID(id); }
}
