package bsmch.CloudsOfSteelPIT.controllers;

import bsmch.CloudsOfSteelPIT.models.Order;
import bsmch.CloudsOfSteelPIT.services.OrderService;
import org.aspectj.weaver.ast.Or;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = {"http://localhost:3000"})
@RestController
@RequestMapping("/order")
public class OrderController {
    @Autowired
    private OrderService orderService;
    @PostMapping("")
    public void addOrder(@RequestBody Order order){
        orderService.addOrder(order);
    }

    @GetMapping("")
    public List<Order> getAllOrders() { return this.orderService.getAll(); }

    @PutMapping("")
    public void updateOrder(@RequestBody Order order) { this.orderService.updateOrder(order); }

    @GetMapping("/notApproved")
    public List<Order> getAllNotApproved() { return this.orderService.getAllNotApproved(); }

    @DeleteMapping("/deleteOrder/{id}")
    public void deleteOrderById(@PathVariable int id) { this.orderService.deleteOrderById(id); }
}
