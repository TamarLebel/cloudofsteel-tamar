package bsmch.CloudsOfSteelPIT.controllers;

import bsmch.CloudsOfSteelPIT.models.PermissionType;
import bsmch.CloudsOfSteelPIT.services.PermissionTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = {"http://localhost:3000"})
@RestController
@RequestMapping("/permission")
public class PermissionTypeController {
    @Autowired
    private PermissionTypeService permissionTypeService;
    @GetMapping("")
    public List<PermissionType> getAll(){return permissionTypeService.getAll();}
}
