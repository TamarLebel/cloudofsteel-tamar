package bsmch.CloudsOfSteelPIT.controllers;
import bsmch.CloudsOfSteelPIT.models.LaunchModel;
import bsmch.CloudsOfSteelPIT.models.launchData;

import bsmch.CloudsOfSteelPIT.services.LaunchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GoldController {
    @Autowired
    LaunchService launchService;

    @PostMapping(value = "/Launch")
    public void catchLaunches(@RequestBody launchData launch) {
        launchService.addLaunch(launch);
    }
}

