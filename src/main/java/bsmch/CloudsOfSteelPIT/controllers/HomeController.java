package bsmch.CloudsOfSteelPIT.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = {"http://localhost:3000/"})

@Controller
public class HomeController {
    @RequestMapping("/")
    public String index() {
        return "/index.html";
    }

}
