package bsmch.CloudsOfSteelPIT.controllers;

import bsmch.CloudsOfSteelPIT.models.InterceptorModel;
import bsmch.CloudsOfSteelPIT.services.InterceptorService;
import bsmch.CloudsOfSteelPIT.services.SupplyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = {"http://localhost:3000"})
@RestController
@RequestMapping("/interceptor")
public class InterceptorController {
    @Autowired
    public InterceptorService interceptorService;

    @Autowired
    public SupplyService supplyService;


    @PostMapping("/addInterceptor")
    public InterceptorModel addInterceptorToDb(@RequestBody InterceptorModel interceptor) {
        return this.interceptorService.addInterceptor(interceptor);
    }

    @GetMapping("")
    public List<InterceptorModel> getAll(){
        return interceptorService.getAllInterceptors();
    }

    @GetMapping("/getInterceptor/{id}")
    public InterceptorModel getInterceptorByID(@PathVariable int id) {
        return interceptorService.getInterceptorById(id);
    }

    @PutMapping("/updateInterceptor")
    public void updateInterceptor(@RequestBody InterceptorModel interceptor) {
        interceptorService.UpdateInterceptor(interceptor);
    }

    @DeleteMapping("/deleteInterceptor/{id}")
    public void deleteInterceptor(@PathVariable int id) {
        interceptorService.deleteInterceptorById(id);
    }
}