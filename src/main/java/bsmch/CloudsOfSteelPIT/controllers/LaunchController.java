package bsmch.CloudsOfSteelPIT.controllers;

import bsmch.CloudsOfSteelPIT.models.LaunchModel;
import bsmch.CloudsOfSteelPIT.services.LaunchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = {"http://localhost:3000"})
@RestController
@RequestMapping("/launches")
public class LaunchController {
    @Autowired
    private LaunchService launchService;
    @GetMapping("")
    public List<LaunchModel> getAll(){
        return launchService.getAllLaunches();
    }
    @GetMapping("/48")
    public List<LaunchModel> getAllAfterDate(){
        return launchService.getLaunchesAfterDate();
    }
}
