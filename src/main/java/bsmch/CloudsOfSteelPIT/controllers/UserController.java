package bsmch.CloudsOfSteelPIT.controllers;

import bsmch.CloudsOfSteelPIT.models.PermissionType;
import bsmch.CloudsOfSteelPIT.services.UserService;
import bsmch.CloudsOfSteelPIT.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.awt.desktop.UserSessionEvent;
import java.util.List;

@CrossOrigin(origins = {"http://localhost:3000"})
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    public UserService userService;

    @GetMapping("")
    public List<User> getAll(){
        return userService.getAll();
    }

    @GetMapping("/{id}")
    public User getUser(@PathVariable int id){
        return userService.getUserById(id);
    }

    @GetMapping("/isValid")
    public int checkifvalid(@RequestParam("name") String name, @RequestParam("password") String password) {
        User user = userService.getUserByNameAndPassword(name, password);
        return user != null ? user.getId() : -1;
    }

    @GetMapping("/permission/{name}")
    public PermissionType getThePermission(@PathVariable String name){
        return userService.getPermissionTypeByName(name);
    }
    @PutMapping("")
    public void updateUser(@RequestBody User user){
        userService.updateUser(user);
    }
    @PostMapping("")
    public boolean addUser(@RequestBody User user){
        return userService.addUser(user);
    }
}
