import React, {useEffect, useState} from "react";
import {getJsonRequest} from "../Services/RequestService";
import {Bar} from 'react-chartjs-2'
import {Pie} from 'react-chartjs-2'
import "./TableData.css";

const LaunchesTable=(props)=>{

        function tableData() {
            function pad(num){
                return num===0?"00":num<10?("0"+num):num
            }
            return <>{props.filteredLaunches.map((launch, index) => {
                const { rocketType, launchDate, interceptor, intercepted, failedReason } = launch
                const date = new Date(launchDate)
                return (
                    <tr key={index}>
                        <td>{rocketType}</td>
                        <td>{date.getDate() + "/" + (date.getMonth()+1) + "/" + date.getFullYear()}</td>
                        <td>{pad(date.getHours()) + ":" + pad(date.getMinutes())+":"+pad(date.getSeconds())}</td>
                        <td>{interceptor.name}</td>
                        <td>{intercepted? "יירוט" : "נפילה"}</td>
                        <td>{failedReason}</td>
                    </tr>
                )
            })}</>
        }
        function tableHeader() {

            return <tr>
                <th>סוג הטיל</th>
                <th>תאריך שיגור</th>
                <th>זמן שיגור</th>
                <th>סוללה מיירטת</th>
                <th>תוצאה</th>
                <th>פירוט</th>
            </tr>

        }

        return(<>
            <div className="bodycontainer scrollable">
                <table className="table tborders table-hover table-condensed table-scrollable" width="100%">
                    <tbody>
                    {tableHeader()}
                    {tableData()}
                    </tbody>
                </table>
            </div>
        </>)

}

const LaunchesBarGraph=({filteredLaunches})=>{
    function foo(arr) {
        let a = [],
            b = [],
            c = [],
            prev;

        arr.sort((m,n)=>m.interceptor.name.localeCompare(n.interceptor.name));
        for (let i = 0; i < arr.length; i++) {
            if (!prev||arr[i].interceptor.name !== prev.interceptor.name) {
                a.push(arr[i].interceptor.name);
                b.push(1);
                c.push(arr[i].intercepted === 1? 1:0);
            } else {
                b[b.length - 1]++;
                if(arr[i].intercepted === 1)
                    c[c.length-1]++
            }
            prev = arr[i];
        }

        return [a, b,c];
    }
    const [labels,launches,interceptions]=foo(filteredLaunches)
    const data = (canvas) => {
        const ctx = canvas.getContext("2d")
        const gradient = ctx.createLinearGradient(0, 0, 100, 0);

        return {
            labels: labels,
            datasets: [{
                label: 'שיגורים',
                backgroundColor: 'rgb(38,38,215)',

                borderWidth: 1,
                data: launches
            }, {
                label: 'יירוטים',
                backgroundColor: 'rgb(255,191,35)',

                borderWidth: 1,
                data: interceptions
            }],
            backgroundColor: gradient

        }
    }
const options={
            responsive: true,
    legend: {
        display: true,
position:"bottom"
    },
    title: {
        display: true,
        fontSize:30,
        text: 'שיגורים ויירוטים בגזרה'
    },
    type:'bar'}

    return (<>
        <Bar data={data}  width={null} height={null} options={options} />
        </>)

}

const LaunchesPie=(props)=>{
    var unIntercepted = props.filteredLaunches.filter(launch => {
        return launch.intercepted === 0
    });
    const labels = ['נחיתה בשטח פתוח', 'סוללה ללא מלאי', 'סוללה מנותקת'];
    var inActive = 0,
        withoutSupply = 0,
        openArea = 0;

    for (let i = 0; i < unIntercepted.length; i ++){
        if(unIntercepted[i].failedReason.trim() === 'סוללה מנותקת')
            inActive ++;
        else if (unIntercepted[i].failedReason.trim() === 'סוללה ללא מלאי')
            withoutSupply ++;
        else openArea ++;
    }

    const datasets = [{
        data : [openArea, withoutSupply, inActive],
        backgroundColor : ['blue', 'orange', 'grey']
    }];
    const data = {
        labels : labels,
        datasets : datasets
    }
    const options={
        responsive: true,
        legend: {
            display: true,
            position:"bottom"
        },
        title: {
            display: true,
            fontSize:30,
            text: 'שיגורים שלא יורטו'
        },
        type:'pie'}


    return(<>
    <div>
        <Pie data={data} width={null} height={null} options={options}/>
    </div>
</>)
}

export default function LaunchData() {
    const [hourRange, setHourRange] = useState(0)
    const [launches, setLaunches] = useState([]);
let interval
    useEffect(() => {
        getJsonRequest("/launches/48").then(data => {
            setLaunches(data)
            setHourRange(48)

        })
        interval=setInterval(()=> {
            getJsonRequest("/launches/48").then(data => {
                setLaunches(data)
            })

        },4500)
        return ()=>clearInterval(interval)
    }, []);

    const [filteredLaunches,setFilteredLaunches] = useState([])
const recalculateLaunches=()=> {
        const newLaunches = launches.filter((val) => {
            const diffTime = Math.abs(new Date(val.launchDate) - Date.now());
            const diffHours = Math.ceil(diffTime / (1000 * 60 * 60));
            return (diffHours <= hourRange)
        }).sort((a, b) => new Date(b.launchDate) - new Date(a.launchDate))
        console.log(newLaunches)
        setFilteredLaunches(newLaunches)
}

    useEffect(() => {
        recalculateLaunches()
    }, [hourRange,launches])

    function handleHourChange(event){
        setHourRange(event.target.value);
    }

    return  <div className="container">
        <select className="custom-select my-1 mr-sm-2" id="inlineFormCustomSelectPref" onChange={handleHourChange}>
            <option value={1}>1 שעות</option>
            <option value={12}>12 שעות</option>
            <option value={24}>24 שעות</option>
            <option value={48} selected>48 שעות</option>
        </select>

        <div className="row">
            <div className="col-12">
                <LaunchesTable filteredLaunches={filteredLaunches}/>
            </div>
        </div>
        <div className="row">
            <div className="col-6">
                <LaunchesPie filteredLaunches={filteredLaunches}/>
            </div>
            <div className="col-6">
                <LaunchesBarGraph filteredLaunches={filteredLaunches}/>
            </div>
        </div>

    </div>
}