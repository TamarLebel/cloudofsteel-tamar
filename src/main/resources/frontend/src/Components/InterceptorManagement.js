import React, {useEffect, useState} from "react";
import InterceptorModal from "./InterceptorModal";
import InterceptorCard from "./InterceptorCard"
import {
    postJsonRequest,
    deleteJsonRequest,
    putMapping,
    getJsonRequest,
    postAndGetJsonRequest
} from "../Services/RequestService";

function InterceptorManagement() {
     const [table, setTable] = useState();

     const getAll = () => {
        getJsonRequest("/supply").then(
            data => {
                return data;
            }).then(data => setTable(data.map(supply =>
            <InterceptorCard key={supply.id} data={supply} setTableItem={updateInterceptorSpecific}
                             updateInterceptor={updateInterceptor} deleteInterceptor={deleteInterceptor}/>)))
     }

    //creating the interceptor list
    useEffect(() => {
        getAll();
    }, []);

    function addInterceptor(newSupply) {
        postAndGetJsonRequest("/interceptor/addInterceptor", newSupply.interceptorModel)
            .then(data =>
                postJsonRequest("/supply/addSupply", {...newSupply, interceptorModel: data})
                ).then(() => getAll());
    }

    const updateInterceptor = (supply) => {
        putMapping("/supply/updateSupply", supply).then(() =>
            putMapping("/interceptor/updateInterceptor", supply.interceptorModel)
                .then(() => getAll())
        );
    }

    function updateInterceptorSpecific(interceptor){
        putMapping("/interceptor/updateInterceptor", interceptor, true);
    }

    const deleteInterceptor = id => {
        getJsonRequest("/supply/getSupplyByInterceptor/" + id).then(data => {
                return data.id;
            }
        ).then(res => deleteJsonRequest("/supply/deleteSupply/" + res)
        ).then(() => deleteJsonRequest("/interceptor/deleteInterceptor/" + id)
        ).then(() => getAll());
    }

    return (
        <>
            <div className="d-flex flex-wrap justify-content-around align-items-center">
                {table}
                <div>
                    <button type="button" className="btn btn-light" data-toggle="modal" data-target="#add">
                        <div className="card mb-3" style={{width: "18rem"}}>
                            <div className="card-header">הוסף סוללה</div>
                            <div className="card-body text-dark">
                                <img src="./images/plus.png" className="card-img-top" alt="..."/>
                            </div>
                        </div>
                    </button>
                </div>
            </div>
            <InterceptorModal modalId="add" title="הוספת סוללה" functionType={addInterceptor}/>
        </>
    )
}

export default InterceptorManagement;