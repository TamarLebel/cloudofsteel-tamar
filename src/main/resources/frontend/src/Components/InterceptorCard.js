import React, {useEffect, useState} from "react";
import InterceptorModal from "./InterceptorModal";

function InterceptorCard(props) {

    const [onSwitch, setOnSwitch] = useState({});
    const [offSwitch, setOffSwitch] = useState({});
    const [isActive, setIsActive] = useState(props.data.interceptorModel.isActive);

    const setClass = () => {
       return  isActive === 1? "success" : "danger"
    }

    useEffect(() => {
        switchTheSwitch();
    }, [])

    const switchTheSwitch = () => {
        if (isActive === 1) {
            setOnSwitch({
                color: "btn btn-success",
                innertext: "on"
            })
            setOffSwitch(
                {
                    color: "btn btn-light",
                    innertext: "-"
                }
            )
        } else {
            setOnSwitch({
                color: "btn btn-light",
                innertext: "-"
            })
            setOffSwitch(
                {
                    color: "btn btn-danger",
                    innertext: "off"
                }
            )
        }
    }

    useEffect(() => {
        const newData = {...props.data.interceptorModel, isActive}
        props.setTableItem(newData);
        switchTheSwitch();
    }, [isActive])

    const toggleONAndOFF = () => {
        setIsActive(isActive === 0 ? 1 : 0);
    }

    const deleteInterceptorFromExistenceForEver = () => {
        props.deleteInterceptor(props.data.interceptorModel.id);
    }

    return (
            <div className={"card border-" + setClass()} style={{width: "18rem"}}>
                <div className={"card-header bg-" + setClass()}>
                    <div className="d-flex align-items-center">
                        <div className="col-8">
                            <span className="card-title">{props.data.interceptorModel.name}</span>
                        </div>
                        <button type="button" className="btn btn-primary border-dark" data-toggle="modal"
                                data-target={"#edit" + props.data.id}><i className="fa fa-pencil"/>
                        </button>
                        <InterceptorModal modalId={"edit"+props.data.id} title="עריכה סוללה" functionType={props.updateInterceptor} supply={props.data}/>
                        <button type="button" className="btn btn-danger border-dark"
                                onClick={deleteInterceptorFromExistenceForEver}><i className="fa fa-times"/>
                        </button>
                    </div>
                </div>

                <div data-toggle="modal" data-target={"#show" + props.data.id}>
                    <img src="./images/battary.jpeg" alt={"Card image cap"} className="card-img-top" />
                </div>
                <InterceptorModal modalId={"show"+props.data.id} title="נתוני סוללה" supply={props.data}/>

                <div className="card-body">
                    <div className="row">
                        <div className="col-4">
                            <a className={"smallheader"}>
                                <bdi>מיקום:</bdi>
                            </a>
                        </div>
                        <div className="col-8">
                            <a className={"realySmallText"}>({props.data.interceptorModel.x}, {props.data.interceptorModel.y}, {props.data.interceptorModel.z})</a>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-4">
                            <a className={"smallheader"}>
                                <bdi>רדיוס:</bdi>
                            </a>
                        </div>
                        <div className="col-3">
                            <a className={"realySmallText"}>{props.data.interceptorModel.radius}</a>
                        </div>
                        <div className="col-5">
                            <div className="btn-group btn-group-sm" role="group">
                                <button type="button" className={onSwitch.color} onClick={toggleONAndOFF}>{onSwitch.innertext}</button>
                                <button type="button" className={offSwitch.color} onClick={toggleONAndOFF}>{offSwitch.innertext}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    );
}

export default InterceptorCard;
