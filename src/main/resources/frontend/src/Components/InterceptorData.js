import React, {useEffect, useState} from "react";
import {getJsonRequest} from "../Services/RequestService";
import {Bar} from 'react-chartjs-2'
import {Pie} from 'react-chartjs-2'
import "./TableData.css";

const InterceptorsTable=({supplies})=>{

    function tableData() {
        const sortedSupplies=supplies.slice()
        sortedSupplies.sort((val1,val2)=>val1.interceptorModel.id-val2.interceptorModel.id)
        return <>{sortedSupplies.map((supply, index) => {
            const { amount,interceptorModel :interceptor,minimum } = supply
            const {isActive,id,x,y,z,name}=interceptor
            const date = new Date(5)
            const status=isActive?
                amount>=minimum?["פעילה","green"]:["מחוברת ללא מלאי","orange"]
                :["מנותקת","red"]
            return (
                <tr key={index}>
                    <td>{name.trim()+id}</td>
                    <td dir="ltr">{`${x}, ${y}, ${z}`}</td>
                    <td>{name}</td>
                    <td>
                        <div>
                            <span>{<span className="circle d-inline-block" style={{background:status[1]}}/>}
                                <span className="d-inline-block">{status[0]}</span></span>
                        </div>
                    </td>
                </tr>
            )
        })}</>
    }
    function tableHeader() {

        return <tr>
            <th>שם הסוללה</th>
            <th>קואורדינטות</th>
            <th>מיקום</th>
            <th>סטטוס</th>

        </tr>

    }

    return(<>
        <div className="bodycontainer scrollable">
            <table className="table tborders table-bordered table-hover table-condensed table-scrollable" width="100%">
                <thead>
                {tableHeader()}
                </thead>
                <tbody>

                {tableData()}
                </tbody>
            </table>
        </div>
    </>)

}

const InterceptorBarGraph=({supplies})=>{
    function foo(arr) {
        let labels = [],
            actives = [],
            notActives = [],
            withoutSupplies = [],
            prev;

        arr.sort((m,n)=>m.interceptor.name.localeCompare(n.interceptor.name));
        for (let i = 0; i < arr.length; i++) {
            if (!prev||arr[i].interceptor.name !== prev.interceptor.name) {
                labels.push(arr[i].interceptor.name);
                if (arr[i].interceptor.isActive && arr[i].amount > arr[i].minimum){
                    actives.push(1);
                    notActives.push(0);
                    withoutSupplies.push(0);
                }
                else if (!arr[i].interceptor.isActive) {
                    actives.push(0);
                    notActives.push(1);
                    withoutSupplies.push(0);
                }
                else if (arr[i].interceptor.isActive && arr[i].amount < arr[i].minimum){
                    actives.push(0);
                    notActives.push(0);
                    withoutSupplies.push(1);
                }
            } else {
                if (arr[i].interceptor.isActive && arr[i].amount > arr[i].minimum){
                    actives[actives.length - 1]++;
                }
                else if (!arr[i].interceptor.isActive){
                    notActives[notActives.length-1]++;
                }
                else if (arr[i].interceptor.isActive && arr[i].amount < arr[i].minimum){
                    withoutSupplies[withoutSupplies.length-1]++;
                }
            }
            prev = arr[i];
        }

        return [labels, actives, notActives, withoutSupplies];
    }
    const [labels,actives,notActives, withoutSupplies]=foo(supplies)
    const data = (canvas) => {
        const ctx = canvas.getContext("2d")
        const gradient = ctx.createLinearGradient(0, 0, 100, 0);

        return {
            labels: labels,
            datasets: [{
                label: 'פעילה',
                backgroundColor: 'rgb(38,215,67)',

                borderWidth: 1,
                data: actives
            }, {
                label: 'מנותקת',
                backgroundColor: 'rgb(255,35,35)',

                borderWidth: 1,
                data: notActives
            }, {
                label: 'מחוברת ללא מלאי',
                backgroundColor: 'rgb(255,193,35)',

                borderWidth: 1,
                data: withoutSupplies

            }],
            backgroundColor: gradient

        }
    }
    const options={
        responsive: true,
        legend: {
            display: true,
            position:"bottom"
        },
        title: {
            display: true,
            fontSize:30,
            text: 'סטטוס סוללות בגזרה'
        },
        type:'bar'}

    return (<>
        <Bar data={data}  width={null} height={null} options={options} />
    </>)

}

const InterceptorsPie=({supplies})=>{
    let activeInterceptors = supplies.filter(supply => {
        return supply.interceptorModel.isActive&&(supply.amount>=supply.minimum)
    }).map((val)=>val.interceptorModel)
        .sort((val,other)=>val.name.localeCompare(other.name))
    console.log(activeInterceptors)

    let bop=new Map()
    activeInterceptors.forEach((val)=>{
        const nameShort=val.name.trim()
        bop.has(nameShort)?bop.set(nameShort,bop.get(nameShort)+1):bop.set(nameShort,1)
    })
    const labels=[...bop.keys()]
    const datasets = [{
        data : [...bop.values()],
        backgroundColor : ['blue', 'orange', 'grey']
    }];
    const data = {
        labels : labels,
        datasets : datasets
    }
    const options={
        responsive: true,
        legend: {
            display: true,
            position:"bottom"
        },
        title: {
            display: true,
            fontSize:30,
            text: 'סוללות פעילות'
        },
        type:'pie'}


    return(<>
        <div>
            <Pie data={data} width={null} height={null} options={options}/>
        </div>
    </>)
}

export default function InterceptorData() {
    const [interceptors, setInterceptors] = useState([]);

    useEffect(() => {
        getJsonRequest("/supply").then(data => {
            setInterceptors(data)
        })
    }, []);




    return interceptors.length>0? <div className="container">
        <div className="row mt-2">
            <div className="col-12">
                <InterceptorsTable supplies={interceptors}/>
            </div>
        </div>
        <div className="row">
            <div className="col-6">
                <InterceptorsPie supplies={interceptors}/>
            </div>
            <div className="col-6">
                <InterceptorBarGraph supplies={interceptors}/>
            </div>
        </div>

    </div>:""
}