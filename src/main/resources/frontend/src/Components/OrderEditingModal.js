import InputModal from "./InputModal";
import React, {useEffect, useState} from "react";
import {putMapping} from "../Services/RequestService";

function OrderEditingModal(props) {

    const [amount, setAmount] = useState(props.order.amount)
    const [order, setOrder] = useState(props.order)
    // const [peice, setPrice] = useState(order.price)
    const rate=props.order.price/props.order.amount

    const updateAmount = (event) =>{
        setAmount(event.target.value);
    }

    const updateOrder = (event) =>{
        let newOrder={...order, amount: amount, price: rate*amount}
        setOrder(newOrder)
        props.update(newOrder)
    }

    return(
        <div id={props.modalId} className="modal" tabIndex="-1">
            <div className="modal-dialog">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title">עריכת הזמנה</h5>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        <div className="form-group row">
                            <h6 htmlFor="inputName" className="col-sm-4 col-form-label">מספר הזמנה</h6>
                            <div className="col-sm-8">
                                <input name="name" placeholder="מספרה זמנה"  value = {order.id} disabled={true}/>
                            </div>
                        </div>
                        <div className="form-group row">
                            <h6 htmlFor="inputName" className="col-sm-4 col-form-label">כמות</h6>
                            <div className="col-sm-8">
                                <input name="name" placeholder="כמות" type="Number" min="1" defaultValue= {amount} onChange={updateAmount} />
                            </div>
                        </div>
                        <div className="form-group row">
                            <h6 htmlFor="inputName" className="col-sm-4 col-form-label">מחיר הזמנה</h6>
                            <div className="col-sm-8">
                                <input name="name" placeholder="מחיר הזמנה"  value={rate*amount} disabled={true}/>
                            </div>
                        </div>
                        <div className="form-group row">
                            <h6 htmlFor="inputName" className="col-sm-4 col-form-label">שם סוללה</h6>
                            <div className="col-sm-8">
                                <input name="name" placeholder="שם סוללה"  value={order.interceptor.name} disabled={true}/>
                            </div>
                        </div>
                        <div className="form-group row">
                            <h6 htmlFor="inputName" className="col-sm-4 col-form-label">משתמש</h6>
                            <div className="col-sm-8">
                                <input name="name" placeholder="משצמש"  value={order.userId.username} disabled={true}/>
                            </div>
                        </div>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" className="btn btn-primary" data-dismiss="modal" onClick={updateOrder}>Save changes</button>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default OrderEditingModal;