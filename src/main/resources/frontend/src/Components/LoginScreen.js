import React, {useEffect, useState} from "react";
import {getJsonRequest, getRequest} from "../Services/RequestService";

function LoginScreen({logChangeFunc}) {

    const [userName, setUserName] = useState("");
    const [password, setPasword] = useState("");
    useEffect(() => {
        if (localStorage.getItem('user') === null)
            localStorage.setItem('user', "");
    });

    const mystyle = {
        backgroundImage: `url("./images/Background.jpg")`,
        backgroundAttachment: 'fixed',
        backgroundRepeat: 'no-repeat',
        width: '100%'
    };

    const mystylein = {
        backgroundColor: "lightgray"
    };

    const getUserName = (event) => {
        setUserName(event.target.value);
    }

    const getPasword = (event) => {
        setPasword(event.target.value);
    }

    const checkIfValid = (event) => {
        getRequest(`/user/isValid?name=${userName}&password=${password}`).then(
            data => {
                if (data !== "-1") {
                    const num = parseInt(data);
                    localStorage.setItem('user', num);
                    getJsonRequest(`/user/permission/${userName}`).then(
                        data => {
                            localStorage.setItem('permission', data.id)
                            console.log(localStorage.getItem("permission"))
                        }
                    );
                    logChangeFunc(true)
                }
            }
        )
    }

    return (
        <div style={mystyle} id={"bg"} className="d-flex justify-content-center align-items-center">
                <div className="col-4">
                    <div style={mystylein}>
                        <h1>ענני הפלדה</h1>
                        <div className="d-flex justify-content-center pt-4">
                            <div className="col-4">
                                <a>שם משתמש</a>
                            </div>
                            <div className="col-4">
                                <input type="text" placeholder="הכנס שם משתמש" onChange={getUserName}/>
                            </div>
                        </div>
                        <div className="d-flex justify-content-center pt-5">
                            <div className="col-4">
                                <a className={"fieldlogintext"}>סיסמא</a>
                            </div>
                            <div className="col-4">
                                <input type="password" placeholder="הכנס סיסמא" onChange={getPasword}/>
                            </div>
                        </div>
                        <div  className={"pt-5 pb-1"}>
                            <button id={"subbutton"} onClick={checkIfValid}>היכנס</button>
                        </div>
                    </div>
                </div>
        </div>
    );
}

export default LoginScreen;