const Swal = require('sweetalert2')

// File for fetch functions
const SERVER_URL = "http://localhost:8080"

export function getJsonRequest(url){
    return fetch(SERVER_URL + url,{method:'get',mode:"cors"}).then((response)=> {
        return response.json();
    }).then((data)=> {
        return data
    }).catch((err)=> {
        Swal.fire({
            icon: 'error',
            title: 'שגיאה בקבלת מידע',
            text: ` משהו השתבש${err.message}`,
        })
    })
}


export function postAndGetJsonRequest(url, newObject,disablePretty) {
    return fetch(SERVER_URL + url, {
        method: "POST",
        mode: "cors",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(newObject)
    }).then(response => {
        Swal.fire(
            'מצויין!',
            'המידע נשמר בהצלחה',
            'success'
        )
        return response.json();
    }).catch(error => {
        Swal.fire({
            icon: 'error',
            title: 'שגיאה בהוספת מידע',
            text: ` משהו השתבש${error.message}`,
        })
    })
}

export function getRequest(url){
    return fetch(SERVER_URL + url,{method:'get',mode:"cors"}).then((response)=> {
        return response.text();
    }).then((data)=> {
        return data
    }).catch((err)=> {
        Swal.fire({
            icon: 'error',
            title: 'שגיאה בקבלת מידע',
            text: ` משהו השתבש${err.message}`,
        })
    })
}

export function postJsonRequest(url, obj){
    return fetch(SERVER_URL + url, {
        method: "POST",
        mode: "cors",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(obj)
    }).then(response => {
        if(!response.ok){
            throw new Error(response)
        }
        Swal.fire(
            'מצויין!',
            'המידע נשמר בהצלחה',
            'success'
        )
    }).catch(error => {
        Swal.fire({
            icon: 'error',
            title: 'שגיאה בהוספת מידע',
            text: ` משהו השתבש${error.message}`,
        })
    })
}

export function deleteJsonRequest(url, id) {
    return fetch(SERVER_URL + url, {
        method:"DELETE",
    }).then((response) => {
        if (!response.ok) {
            throw Error(response.status + " " + response.statusText);
        }
        Swal.fire(
            'מצויין!',
            'המידע נמחק בהצלחה',
            'success'
        )
    }).catch((error) => {
        Swal.fire({
            icon: 'error',
            title: 'שגיאה במחיקת מידע',
            text: ` משהו השתבש ${error.message}`,
        })
    })
}

export function putMapping(url,json, disableAlert) {
    return fetch(SERVER_URL + url,
        {method: 'put',
            mode: "cors",
            headers: {
            'Content-Type': 'application/json'},
            body:JSON.stringify(json)

    }).then((response) => {
        if(!response.ok){
            throw new Error(response)
        }
        if(!disableAlert){
            Swal.fire(
                'מצויין!',
                'המידע עודכן בהצלחה',
                'success'
            )
        }
    }).catch(error => {
        Swal.fire({
            icon: 'error',
            title: 'שגיאה בעדכון מידע',
            text: ` משהו השתבש ${error.message}`,
        })

    })
}
