# import urllib
from urllib.parse import urlencode
from urllib.request import urlopen, Request
import json
import random
import time
import math
import ctypes
import os


STD_INPUT_HANDLE = -10
STD_OUTPUT_HANDLE = -11
STD_ERROR_HANDLE = -12

FOREGROUND_BLUE = 0x09
FOREGROUND_GREEN = 0xA
FOREGROUND_RED = 0xC
FOREGROUND_NORMAL = 0x07
FOREGROUND_YELLOW = 0xE
FOREGROUND_INTENSITY = 0x08


std_out_handle = ctypes.windll.kernel32.GetStdHandle(STD_OUTPUT_HANDLE)


def set_color(color, handle=std_out_handle):
    return ctypes.windll.kernel32.SetConsoleTextAttribute(handle, color)


CONST_SOURCES = [
    {"x": -34.78903345691955, "y": 2.9576912504549386, "z": -182.02369831595473},
    {"x": -31.704723834475512, "y": 2.994115321960035, "z": -185.18510258294558},
    {"x": -31.028718822704043, "y": 2.8183301528867135, "z": -187.42193534112513},
    {"x": -28.872275111534226, "y": 2.999999999999998, "z": -190.64209577476032},
    {"x": -28.924354765075325, "y": 3.386976753447979, "z": -185.8031499706906},
    {"x": -26.917633782524778, "y": 3.0135695823194175, "z": -190.23950847337872},
    {"x": -28.775153222562192, "y": -1.4079489426575098e-15, "z": -193.65916166649095},
    {"x": -26.114909046856855, "y": 2.6461556502668766, "z": -197.06977945212114},
]
MISSILE_TYPE = [
     "Grad",
    "Katyusha",
    "kasam",
    "Fajr"
]
ISRAEL_POLY = [
    {"x": -6, "y": -110},
    {"x": 16, "y": -180},
    {"x": 16, "y": -235},
    {"x": 35, "y": -255},
    {"x": 40, "y": -280},
    {"x": 16, "y": -265},
    {"x": -5, "y": -270},
    {"x": -5, "y": -255},
    {"x": 0, "y": -260},
    {"x": 0, "y": -240},
    {"x": -5, "y": -240},
    {"x": -15, "y": -210},
    {"x": -25, "y": -190},
    {"x": -35, "y": -180},
]

CONST_SPEED = 5
CONST_G = 0.45


def randf(min, max):
    return random.random() * (max - min) + min


def countBackwards(seconds):
    for i in range(seconds, 0, -1):
        if 5 >= i:
            set_color(FOREGROUND_RED)
        print(str(i) + "," + time.sleep(1))
    set_color(FOREGROUND_NORMAL)


def checkIfInIsrael(position):
    count = 0
    for i in range(0, len(ISRAEL_POLY)):
        firstPoint = ISRAEL_POLY[i]
        secondPoint = ISRAEL_POLY[(i + 1) % len(ISRAEL_POLY)]
        if (position["x"] >= firstPoint["x"] and position["x"] <= secondPoint["x"]) or (
            position["x"] >= secondPoint["x"] and position["x"] <= firstPoint["x"]
        ):
            yInFallPosition = firstPoint["y"] + (
                float((secondPoint["y"] - firstPoint["y"]))
                / (secondPoint["x"] - firstPoint["x"])
            ) * (position["x"] - firstPoint["x"])
            if yInFallPosition < position["z"]:
                count += 1
    return count % 2 == 1


def calculateFallingPlace(missile):
    timeLeft = (
        math.sqrt(math.pow(missile["vy"], 2) + 2 * CONST_G * missile["origin"]["y"])
        + missile["vy"]
    ) / CONST_G
    return {
        "x": missile["origin"]["x"] + timeLeft * missile["vx"],
        "y": missile["origin"]["y"]
        + timeLeft * missile["vy"]
        - (CONST_G / 2) * math.pow(timeLeft, 2),
        "z": missile["origin"]["z"] + timeLeft * missile["vz"],
    }


def atpMode(host, port):

    ctypes.windll.user32.SetWindowPos(
        ctypes.windll.user32.GetForegroundWindow(), -1, 0, 0, 320, 320, 0
    )

    # Case 1
    set_color(FOREGROUND_BLUE)
    print("============== CASE 1 ==============")
    set_color(FOREGROUND_NORMAL)
    shoot1 = {
        "origin": {
            "y": 2.999999999999998,
            "x": -28.872275111534226,
            "z": -190.64209577476032,
        },
        "ver_angle": 1.3708670996723367,
        "vx": 0.41527208752089356,
        "vy": 4.900403178934752,
        "vz": 0.9019965505544965,
        "hor_angle": 1.1393340113017227,
    }
    sendLaunch(host, port, shoot1)
    print("Missile sent")

    countBackwards(20)

    # Case 2
    set_color(FOREGROUND_BLUE)
    print("============== CASE 2 ==============")
    set_color(FOREGROUND_NORMAL)
    shoot2 = {
        "origin": {
            "y": 2.994115321960035,
            "x": -31.704723834475512,
            "z": -185.18510258294558,
        },
        "ver_angle": 1.254224618082116,
        "vx": 1.080017473582516,
        "vy": 4.75154131753258,
        "vz": 1.120900247362444,
        "hor_angle": 0.803971357779294,
    }
    sendLaunch(host, port, shoot2)
    print("Missile sent")

    countBackwards(20)

    # Case 3
    set_color(FOREGROUND_BLUE)
    print("============== CASE 3 ==============")
    set_color(FOREGROUND_NORMAL)
    set_color(FOREGROUND_GREEN)
    print("disable all interceptors")
    print("Press ENTER key to continue")
    input()
    set_color(FOREGROUND_NORMAL)
    countBackwards(10)
    shoot3 = [
        {
            "origin": {
                "y": 2.8183301528867135,
                "x": -31.028718822704043,
                "z": -187.42193534112513,
            },
            "ver_angle": 1.2532063584050077,
            "vx": 1.4185259360353193,
            "vy": 4.749953880025136,
            "vz": -0.6524739890825381,
            "hor_angle": 5.852074461089717,
        },
        {
            "origin": {
                "y": 2.8183301528867135,
                "x": -31.028718822704043,
                "z": -187.42193534112513,
            },
            "ver_angle": 1.3740450295985391,
            "vx": 0.9745260577424103,
            "vy": 4.903534112384289,
            "vz": -0.07518092487234289,
            "hor_angle": 6.206191667542835,
        },
        {
            "origin": {
                "y": 3.0135695823194175,
                "x": -26.917633782524778,
                "z": -190.23950847337872,
            },
            "ver_angle": 1.3943908823451294,
            "vx": 0.7382590765327164,
            "vy": 4.922404335587983,
            "vz": 0.47424581484888306,
            "hor_angle": 0.5710026171587065,
        },
        {
            "origin": {
                "y": 2.9576912504549386,
                "x": -34.78903345691955,
                "z": -182.02369831595473,
            },
            "ver_angle": 1.2522308668586613,
            "vx": 1.5554440129639961,
            "vy": 4.748428497717548,
            "vz": -0.18171605487224826,
            "hor_angle": 6.166886646335916,
        },
    ]
    for currShoot in shoot3:
        sendLaunch(host, port, currShoot)
        print("Missile sent")

    countBackwards(28)

    # Case 4

    set_color(FOREGROUND_BLUE)
    print("============== CASE 4 ==============")
    set_color(FOREGROUND_NORMAL)
    set_color(FOREGROUND_GREEN)
    print("enable Jerusalem interceptor")
    print("Press ENTER key to continue")
    input()
    set_color(FOREGROUND_NORMAL)

    countBackwards(10)

    shoot4 = {
        "origin": {
            "y": 2.6461556502668766,
            "x": -26.114909046856855,
            "z": -197.06977945212114,
        },
        "ver_angle": 1.3066780228620751,
        "vx": 0.4056096328601511,
        "vy": 4.826615250487386,
        "vz": 1.2406716122704027,
        "hor_angle": 1.254822084823862,
    }
    sendLaunch(host, port, shoot4)
    print("Missile sent")

    # Case 5
    set_color(FOREGROUND_BLUE)
    print("============== CASE 5 ==============")
    set_color(FOREGROUND_NORMAL)
    set_color(FOREGROUND_GREEN)
    print("You need to update the DB")
    print("Press ENTER key to continue")
    input()
    set_color(FOREGROUND_NORMAL)
    shoot5 = {
        "origin": {
            "y": -1.4079489426575098e-15,
            "x": -28.775153222562192,
            "z": -193.65916166649095,
        },
        "ver_angle": 1.2005665152723184,
        "vx": 1.808704396683117,
        "vy": 4.661221087970499,
        "vz": -0.04007960177300042,
        "hor_angle": 6.261029645022264,
    }
    sendLaunch(host, port, shoot5)
    print("Missile sent")

    countBackwards(25)

    # Case 6
    set_color(FOREGROUND_BLUE)
    print("============== CASE 6 ==============")
    set_color(FOREGROUND_NORMAL)
    shoot6 = {
        "origin": {
            "y": 2.999999999999998,
            "x": -28.872275111534226,
            "z": -190.64209577476032,
        },
        "ver_angle": 1.3753630746601437,
        "vx": 1.3460907248518425,
        "vy": 4.904818138561646,
        "vz": 2.81833773834585946,
        "hor_angle": 0.22680798980003328,
    }
    sendLaunch(host, port, shoot6)

    countBackwards(25)

    # Case 7
    set_color(FOREGROUND_BLUE)
    print("============== CASE 7 ==============")
    set_color(FOREGROUND_NORMAL)
    set_color(FOREGROUND_GREEN)
    print("disable all interceptors")
    print("enable Be'er Sheva interceptor")
    print("Press ENTER key to continue")
    input()
    set_color(FOREGROUND_NORMAL)

    countBackwards(10)

    shoot7 = [
        {
            "origin": {
                "y": 2.6461556502668766,
                "x": -26.114909046856855,
                "z": -197.06977945212114,
            },
            "ver_angle": 1.35089105687364,
            "vx": 0.9264045911180634,
            "vy": 4.879590589306025,
            "vz": 0.5756476477077452,
            "hor_angle": 0.5559906581354702,
        },
        {
            "origin": {
                "y": 2.6461556502668766,
                "x": -26.114909046856855,
                "z": -197.06977945212114,
            },
            "ver_angle": 1.2959966507271123,
            "vx": 0.640698816006097,
            "vy": 4.8123978783357995,
            "vz": 1.195964751887565,
            "hor_angle": 1.0789849332792492,
        },
        {
            "origin": {
                "y": 3.386976753447979,
                "x": -28.924354765075325,
                "z": -185.8031499706906,
            },
            "ver_angle": 1.3808834197344129,
            "vx": 0.9045900606974564,
            "vy": 4.910103398346137,
            "vz": 0.2694465431900967,
            "hor_angle": 0.28949776719294684,
        },
        {
            "origin": {
                "y": 2.994115321960035,
                "x": -31.704723834475512,
                "z": -185.18510258294558,
            },
            "ver_angle": 1.2680354326041812,
            "vx": 0.8769679829290994,
            "vy": 4.77258474549453,
            "vz": 1.2055546457917934,
            "hor_angle": 0.9418910172113836,
        },
        {
            "origin": {
                "y": 2.8183301528867135,
                "x": -31.028718822704043,
                "z": -187.42193534112513,
            },
            "ver_angle": 1.2380409257617462,
            "vx": 1.613818608899487,
            "vy": 4.725729424387094,
            "vz": 0.2511392144842172,
            "hor_angle": 0.15437973856331594,
        },
        {
            "origin": {
                "y": -1.4079489426575098e-15,
                "x": -28.775153222562192,
                "z": -193.65916166649095,
            },
            "ver_angle": 1.2519227408634497,
            "vx": 0.5644858282736772,
            "vy": 4.747945740039462,
            "vz": 1.462315629171522,
            "hor_angle": 1.2023978247641023,
        },
    ]
    for currShoot in shoot7:
        sendLaunch(host, port, currShoot)
        print("Missile sent")

    set_color(FOREGROUND_BLUE)
    print("===============  END ===============")
    set_color(FOREGROUND_NORMAL)
    # Random launches
    set_color(FOREGROUND_GREEN)
    print("Generating random launches in ")
    set_color(FOREGROUND_NORMAL)
    countBackwards(30)
    regularMode(host, port)


def sendLaunch(host, port, data):
    print(json.dumps(data).encode())
    req = Request("http://" + host + ":" + port + "/Launch", json.dumps(data).encode())
    req.add_header("Content-Type", "application/json")
    urlopen(req)


def regularMode(host, port):
    while True:
        ver_angle = random.random() * (math.pi / 2.25 - math.pi / 2.8) + math.pi / 2.8
        hor_angle = random.random() * math.pi * 2
        data = {
            "origin": random.choice(CONST_SOURCES),
            "ver_angle": ver_angle,
            "hor_angle": hor_angle,
            "vy": CONST_SPEED * math.sin(ver_angle),
            
            "vx": CONST_SPEED * math.cos(ver_angle) * math.cos(hor_angle),
            "vz": CONST_SPEED * math.cos(ver_angle) * math.sin(hor_angle),
            "missile_type" : random.choice(MISSILE_TYPE)
        }

        if checkIfInIsrael(calculateFallingPlace(data)):
            sendLaunch(host, port, data)
            print("Missile sent")
            time.sleep(randf(3, 6))


def main():
    set_color(FOREGROUND_YELLOW)
    print(
        "==============================================================================="
    )
    print("		       	       *******************")
    print("		       	       ***Gold Detector***")
    print("		       	       *******************")
    print(
        "==============================================================================="
    )
    print
    set_color(FOREGROUND_NORMAL)
    print("Enter host pc name:")
    host = input()
    print("Enter port:")
    port = input()
    print("Gold detector has been activated")
    print("--------------------------------")

    if "@ATPMode" in host:
        atpMode(host.split("@")[0], port)
    else:
        regularMode(host, port)


if __name__ == "__main__":
    main()
    print("BYE BYE")
