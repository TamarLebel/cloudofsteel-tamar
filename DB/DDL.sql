DROP TABLE IF EXISTS T_INTERCEPTORS CASCADE;
DROP TABLE IF EXISTS T_SUPPLY CASCADE;
DROP TABLE IF EXISTS T_LAUNCHES CASCADE;
DROP TABLE IF EXISTS T_PERMISSIONS_TYPES CASCADE;
DROP TABLE IF EXISTS T_USERS CASCADE;
DROP TABLE IF EXISTS T_ORDERS CASCADE;
DROP TABLE IF EXISTS T_PROFILES CASCADE;

CREATE TABLE T_INTERCEPTORS
(
  INTERCEPTOR_ID       serial			        PRIMARY KEY,
  INTERCEPTOR_NAME     character(30)        NOT NULL,
  X_POSITION           numeric                   NOT NULL,
  Y_POSITION           numeric                   NOT NULL,
  Z_POSITION           numeric                   NOT NULL,
  DEFENSIVE_RADIUS     numeric                   NOT NULL,
  IS_ACTIVE            numeric                   NOT NULL
);

CREATE TABLE T_SUPPLY
(
  SUPPLY_ID        serial                       PRIMARY KEY,
  AMOUNT           numeric                       NOT NULL,
  MINIMUM		   numeric                       NOT NULL,
  PRICE            numeric                       NOT NULL,
  INTERCEPTOR_ID   integer           NOT NULL
);

CREATE TABLE T_LAUNCHES
(
  LAUNCH_ID                 serial                       PRIMARY KEY,
  ROCKET_TYPE               character(30)                 NOT NULL,
  LAUNCH_DATE               timestamp with time zone      NOT NULL,
  INTERCEPTOR_ID            integer,
  INTERCEPTED               numeric(1)                       NOT NULL,
  FAILED_INTERCEPTED_REASON character(60) 
);

CREATE TABLE T_PERMISSIONS_TYPES
(
  PERMISSION_TYPE_ID  numeric            PRIMARY KEY,
  PERMISSION_NAME     character(30) NOT NULL
);

CREATE TABLE T_USERS
(
  USER_ID     		  serial                    PRIMARY KEY,
  USER_NAME           character(30)         NOT NULL,
  PASSWORD            character(30)         NOT NULL,
  PERMISSION_TYPE_ID  numeric                    NOT NULL
);
  
CREATE TABLE T_PROFILES
(
  PROFILE_ID          serial  					PRIMARY KEY,
  USER_ID     		  integer                    ,
  FIRST_NAME          character(30)         ,
  LAST_NAME	 		  character(30)         ,
  AGE				  numeric                    ,
  City				  character(30)         ,
  CELLPHONE			  character(30)         ,
  EMAIL 			  character(30)         
);

CREATE TABLE T_ORDERS
(
  ORDER_ID         serial                       PRIMARY KEY,
  AMOUNT           numeric                       NOT NULL,
  PRICE            numeric                       NOT NULL,
  IS_APROVED       numeric                       NOT NULL,
  USER_ID  		   integer                       NOT NULL,
  INTERCEPTOR_ID   integer            		    NOT NULL
);

ALTER TABLE T_SUPPLY ADD CONSTRAINT FK_INV_INTERCEPTOR_ID
FOREIGN KEY (INTERCEPTOR_ID)
REFERENCES T_INTERCEPTORS ON DELETE CASCADE;


ALTER TABLE T_LAUNCHES ADD CONSTRAINT FK_LAUNCH_INTERCEPTOR
FOREIGN KEY (INTERCEPTOR_ID)
REFERENCES T_INTERCEPTORS ON DELETE CASCADE;


ALTER TABLE T_USERS ADD CONSTRAINT FK_USER_PERMISSION
FOREIGN KEY (PERMISSION_TYPE_ID)
REFERENCES T_PERMISSIONS_TYPES ON DELETE CASCADE;


ALTER TABLE T_ORDERS ADD CONSTRAINT FK_ORDER_INTERCEPTOR
FOREIGN KEY (INTERCEPTOR_ID)
REFERENCES T_INTERCEPTORS ON DELETE CASCADE;


ALTER TABLE T_ORDERS ADD CONSTRAINT fk_order_user
FOREIGN KEY (USER_ID)
REFERENCES T_USERS ON DELETE CASCADE;


ALTER TABLE T_PROFILES ADD CONSTRAINT fk_PROFILE_user
FOREIGN KEY (USER_ID)
REFERENCES T_USERS ON DELETE CASCADE;

