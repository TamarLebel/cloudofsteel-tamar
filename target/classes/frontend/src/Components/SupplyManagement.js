import React, {useEffect, useState} from "react";
import {getJsonRequest, putMapping} from "../Services/RequestService.js"
import {postJsonRequest} from "../Services/RequestService";

export default function SupplyManagement(props) {
    const [supplies, setSupplies] = useState()

    function getSupplies() {
        getJsonRequest("/supply")
            .then((data) => {
                console.log(data)
                setSupplies(data)
            })
    }

    useEffect(() => {
        getSupplies()
    }, [])

    const sumbitOrder=([supply,value])=>{
        console.log(supply)
        console.log(value)
        console.log(localStorage.getItem("user"))
        let newOrder={amount:value,price:value*supply.price,isApproved:0,interceptor:supply.interceptorModel}
        getJsonRequest(`/user/${localStorage.getItem("user")}`).then(data=>postJsonRequest('/order',{...newOrder,userId:data}))
    }
    const cards = supplies ? supplies.map((val) => {
        const onSubmit=(event)=>{
            event.preventDefault();
            sumbitOrder( [val,event.target.numToOrder.value])
        }
        return <div className="col-3" key={val.id}>
            <div className="card" style={{borderColor:val.amount<val.minimum?'rgba(253,150,150,0.5)':'rgba(5,181,194,0.5)'}}>
                <div className="card-header" style={{backgroundColor:val.amount<val.minimum?'rgba(220,56,56,0.5)':'rgba(31,200,213,0.5)'}}>
                    <div className="d-flex flex-row">
                        <div className="ml-auto"> {val.interceptorModel.name}</div>

                    </div>
                </div>
                <div className="card-body">
                    <img className="card-img-top" src="./images/battary.jpeg" alt="Card image cap"/>
                    <div className="text-right">
                        <span dir="ltr">
                            מלאי: {val.amount}
                            <br/>
                            מינימום דרוש לסוללה: {val.minimum}
                        </span>
                        <form className="form-inline" onSubmit={onSubmit}>

                                <input type="number" className="w-50 form-control" id="numToOrder"
                                       name="numToOrder" placeholder=""/>
                            <button type="submit" className="mr-2 btn" style={{backgroundColor:val.amount<val.minimum?'rgb(220,56,56)':'rgb(31,200,213)'}}>הזמן</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    }) : null

    return (
        <div className="mt-3 container">
            <div className="row">{cards}</div>
        </div>
    )
}