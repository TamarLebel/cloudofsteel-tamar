import React, {useEffect, useState} from "react";
import OrderTable from "./OrderTable";

import './TableData.css'
import {getJsonRequest, postAndGetJsonRequest, deleteJsonRequest, putMapping} from "../Services/RequestService";

const UserTable = ({users,reimport,admin}) => {

    function tableData() {


        return <>{users.length>0&&users.map((user, index) => {
            const { username,permissionType } =user
            const {id:permissionNum}=permissionType
            const makeAdmin=(event)=>{
                event.preventDefault()
                putMapping("/user",{...user,permissionType:admin}).catch((e)=>console.log(e.message))
                reimport()
            }
            const status = permissionNum !== 1? (<button onClick={makeAdmin} className="btn btn-light">*</button>) : ""

            return (
                <tr key={index}>
                    <td>{username}</td>
                    <td>{permissionNum}</td>
                    <td>{status}</td>
                </tr>
            )
        })}</>
    }
    function tableHeader() {

        return <tr>
            <th>שם משתמש</th>
            <th>רמת הרשאות</th>
            <th>שדרג למנהל</th>
        </tr>

    }

    return(<>
        <div className="bodycontainer scrollable">
            <table className="table table-striped table-hover table-condensed table-scrollable" width="100%">
                <thead>
                {tableHeader()}
                </thead>
                <tbody>
                {tableData()}
                </tbody>
            </table>
        </div>
    </>)

}
const UserAdder = ({reimport,perms}) => {
    const onSubmit=(e)=>{
        e.preventDefault()
        const newUser={
            username:e.target.name.value,
            password:e.target.password.value,
            permissionType:perms.find(val=> val.id === e.target.permission.value)
        }
        e.target.name.value=""
        e.target.password.value=""
        postAndGetJsonRequest("/user",newUser).then((res)=> {
            !res? alert("הכנסה נכשלה, שם משתמש זה קיים כבר"):alert("משתמש הוכנס בהצלחה!")
            reimport()
        })


    }
    return <form onSubmit={onSubmit}><table className="table table-striped table-condensed bodycontainer" width="100%">
        <thead>
        <tr>
            <th>
                שם משתמש
            </th>
            <th>
                סיסמה
            </th>
            <th>
                רמת הרשאות
            </th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>
                <input type="text" className="form-control" id="validationServer01" placeholder="First name"
                       name="name" required/>
            </td>
            <td>
                <input type="text" className="form-control" id="validationServer01" placeholder="First name"
                       name="password" required/>
            </td>
            <td>
                <div className="d-flex flex-row">
                    <div className="ml-5">
                        <select name="permission" className="form-control">
                            {perms.length>0&&perms.map((val)=>{return <option key={val.id}>{val.id}</option>})}
                        </select>
                    </div>
                    <div className="mr-5">
                        <button type="submit" className="btn btn-light">הוסף</button>
                    </div>
                </div>
            </td>
        </tr>
        </tbody>
    </table></form>
}
export default function ManagerOptions() {

    const [orders, setOrders] = useState([]);

    useEffect(() => {
        importOrderData();
    },[]);

    const importOrderData = () =>{
        getJsonRequest("/order/notApproved").then(data => {
            setOrders(data);
        });
    }

    function approveOrder(order) {
        order.isApproved = 1;
        putMapping("/order", order).then(data => getJsonRequest("/order/notApproved")).then(data => {
            setOrders(data);
        }).then(() => getJsonRequest("/supply/getByInterceptorId/" + order.interceptor.id).then(data => {
            let supply = data;
            supply.amount += order.amount;
            return supply;
        }).then(res => putMapping("/supply/updateSupply", res)))
    }

    function deleteOrder(id) {
        deleteJsonRequest("/order/deleteOrder/" + id, id).then(() => getJsonRequest("/order/notApproved")).then(data => {
            setOrders(data);
        });
    }

    const [users,setUsers]=useState([])
    const [perms,setPerms]=useState([])
    const [admin,setAdmin]=useState([])

    const getUsers=()=>{
        getJsonRequest("/user").then((data)=> {
            setUsers(data)
        })
    }
    useEffect(()=>{
        getUsers()
        getJsonRequest("/permission").then(data=> {
            setAdmin(data.find((val)=> val.id === 1))
            setPerms(data)
        })
    },[])
    return <div className="m-3">
        <div>
            <OrderTable orders = {orders} approve = {approveOrder} update={importOrderData} delete = {deleteOrder}/>
        </div>
        <div className="my-3">
            <UserTable users={users} admin={admin} reimport={getUsers}/>
        </div>
        <div className="my-4">
            <UserAdder reimport={getUsers} perms={perms}/>
        </div>
    </div>
}
