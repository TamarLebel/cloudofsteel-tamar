import React, {useEffect, useState} from "react";
import OrderEditingModal from "./OrderEditingModal"
import {putMapping} from "../Services/RequestService";

const OrderTable = (props) => {

    const [orderStatus, setOrderStatus] = useState({})

    // useEffect(() =>
    // {
    //     putMapping("/order", orderStatus).then(props.update)
    // },[orderStatus])

    const updateOrder = (order) =>{
        // setOrderStatus(order)
        putMapping("/order", order).then(()=>props.update())
    }

    function tableData() {

        return <>{props.orders.map((order, index) => {
            const { id, amount , price, interceptor, userId } = order
            return (
                <tr key={id}>
                    <td>{id}</td>
                    <td>{amount}</td>
                    <td>{price}</td>
                    <td>{interceptor.name}</td>
                    <td>{userId.username}</td>
                    <td>
                        <button onClick={() =>props.approve(order)}>אשר</button>
                        <button data-toggle="modal" data-target={"#editOrder" + id}>עדכן</button>
                        <button onClick={() =>props.delete(id)}>מחק</button>
                    </td>
                    <OrderEditingModal modalId={"editOrder" + id} order={order} update={updateOrder}/>
                </tr>
            )
        })}</>
    }

    function tableHeader() {

        return <tr>
            <th>מס' הזמנה</th>
            <th>כמות</th>
            <th>מחיר</th>
            <th>סוללה</th>
            <th>משתמש</th>
            <th></th>
        </tr>

    }

    return(<>
        <div className="bodycontainer scrollable">
            <table className="table table-hover table-condensed table-scrollable" width="100%">
                <tbody>
                {props.orders.length > 0 ? <>{tableHeader()} {tableData()}</> : ""}
                </tbody>
            </table>
        </div>
    </>)

}
export default OrderTable;