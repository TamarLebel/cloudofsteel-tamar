import React, {useEffect, useState} from "react";
import InputModal from "./InputModal";

const initialValues = {
    minimum: "", price: "", interceptorModel: {id: "", name: "", x: "", y: "", z: "", radius: "", isActive: 0}
}

function InterceptorModal (props){
    const isShow = !props.functionType;
    const [supply, setSupply] = useState(initialValues);

     useEffect(() => {
         if (props.supply) {setSupply(props.supply);}
     },[]);

    const changeSupply = e => {
        const {name, value} = e.target;
        setSupply({...supply, [name]: value,});
    };

    const changeInterceptor = e => {
        const {name, value} = e.target;
        const interceptorModel = {...supply.interceptorModel, [name]: value};
        setSupply({...supply, interceptorModel});
    };

    return (
        <div className="modal fade" id={props.modalId} data-backdrop="static" tabIndex="-1" role="dialog"
             aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div className="modal-dialog" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h5 className="modal-title ml-auto" id="staticBackdropLabel">{props.title}</h5>
                    </div>
                    <div className="modal-body">
                        <form>
                            <div className="form-group row">
                                <label htmlFor="inputName" className="col-sm-2 col-form-label">שם סוללה</label>
                                <div className="col-sm-10">
                                    {isShow? supply.interceptorModel.name : <InputModal name="name" placeholder="שם סוללה" onChange={changeInterceptor}
                                                value = {supply.interceptorModel.name} isShow={isShow}/>}
                                </div>
                            </div>
                            <div className="form-group row">
                                <label className="col-sm-2 col-form-label">מיקום</label>
                                <div className="col-sm-3">
                                    {isShow? supply.interceptorModel.x :<InputModal name="x" placeholder="x" onChange={changeInterceptor}
                                                 value={supply.interceptorModel.x} isShow={isShow}/>}
                                </div>
                                <div className="col-sm-3">
                                    {isShow? supply.interceptorModel.y :<InputModal name="y" placeholder="y" onChange={changeInterceptor}
                                                 value={supply.interceptorModel.y} isShow={isShow}/>}
                                </div>
                                <div className="col-sm-3">
                                    {isShow? supply.interceptorModel.z :<InputModal name="z" placeholder="z" onChange={changeInterceptor}
                                                 value={supply.interceptorModel.z} isShow={isShow}/>}
                                </div>
                            </div>
                            <div className="form-group row">
                                <label htmlFor="inputRadius" className="col-sm-2 col-form-label">רדיוס</label>
                                <div className="col-sm-10">
                                    {isShow ? supply.interceptorModel.radius:<InputModal name="radius" placeholder="הכנס רדיוס פעולה"
                                                          onChange={changeInterceptor}
                                                          value={supply.interceptorModel.radius} isShow={isShow}/>}
                                </div>
                            </div>

                            {
                                isShow ?
                                    <div className="form-group row">
                                        <label htmlFor="amount" className="col-sm-2 col-form-label">כמות טילים</label>
                                        <div className="col-sm-10">
                                            {isShow ? supply.amount:<InputModal name="amount" placeholder="הכנס כמות מינימלית"
                                                                  value={supply.amount} isShow={isShow}/>}
                                        </div>
                                    </div>
                                    : ""
                            }

                            <div className="form-group row">
                                <label htmlFor="inputMinimalAmount" className="col-sm-2 col-form-label">כמות
                                    מינימלית</label>
                                <div className="col-sm-10">
                                    {isShow ? supply.minimum:<InputModal name="minimum" placeholder="הכנס כמות מינימלית" onChange={changeSupply}
                                                value = {supply.minimum} isShow={isShow}/>}
                                </div>
                            </div>
                            <div className="form-group row">
                                <label htmlFor="inputPrice" className="col-sm-2 col-form-label">מחיר לטיל</label>
                                <div className="col-sm-10">
                                    {isShow ? supply.price:<InputModal name="price" placeholder="הכנס מחיר לטיל" onChange={changeSupply}
                                                value = {supply.price} isShow={isShow}/>}
                                </div>
                            </div>
                        </form>

                        {
                            !isShow ?
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-secondary" data-dismiss="modal">בטל
                                    </button>
                                    <button type="button" className="btn btn-primary" data-dismiss="modal"
                                            onClick={() => props.functionType(supply)}>אשר
                                    </button>
                                </div>
                                : ""
                        }
                    </div>
                </div>
            </div>
        </div>
    )
}

export default InterceptorModal;