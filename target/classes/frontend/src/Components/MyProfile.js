import React, {useEffect, useState} from "react";
import {getJsonRequest,putMapping} from "../Services/RequestService.js"

export default function MyProfile() {
    const [user, setUser] = useState()

    function getUserData() {
        const id=localStorage.getItem("user")
        getJsonRequest(`/profile/userid=${id}`)
            .then((data) => {
                console.log(data)
                setUser(data)
            })
    }

    useEffect(() => {
        getUserData()
    }, [])

    function onChange(e) {
        e.preventDefault()
        const {name,value}=e.target
        setUser((prevState) => {

            prevState[name] = value
            console.log(prevState)
            return prevState
        })
    }
    function onSubmit(e) {
        e.preventDefault()
        putMapping("/profile", user).then(r =>{console.log(r)})
    }

    return (<div className="w-25 mt-3 mx-auto">
        {user ? (<bdi>
            <form onSubmit={onSubmit}>
                <div className="form-group text-center">
                    <label htmlFor="exampleFormControlInput1">שם פרטי:</label>
                    <input type="text" className="form-control" name="firstname" defaultValue={user.firstname.trim()} maxLength={30}
                           onInput={onChange}/>
                </div>
                <div className="form-group text-center">
                    <label htmlFor="exampleFormControlInput1">שם משפחה:</label>
                    <input type="text" className="form-control" name="lastname" defaultValue={user.lastname.trim()} maxLength={30}
                           onChange={onChange}/>
                </div>
                <div className="form-group text-center">
                    <label>גיל:</label>
                    <input type="number" className="form-control" name="age" defaultValue={user.age}
                           onChange={onChange}/>
                </div>
                <div className="form-group text-center">
                    <label>עיר מגורים:</label>
                    <input type="text" className="form-control" name="city" defaultValue={user.city.trim()} maxLength={30}
                           onChange={onChange}/>
                </div>
                <div className="form-group text-center">
                    <label>מספר נייד:</label>
                    <input type="text" className="form-control" name="cellphone" defaultValue={user.cellphone.trim()} maxLength={30}
                           onChange={onChange}/>
                </div>
                <div className="form-group text-center">
                    <label>כתובת מייל:</label>
                    <input type="email" className="form-control" name="email" defaultValue={user.email.trim()} maxLength={30}
                           onChange={onChange}/>
                </div>
                <div className="form-group text-center">
                    <button type="submit" className="btn btn-light">עדכן</button>

                </div>
            </form>
        </bdi>) : "טוען"}
    </div>)
}