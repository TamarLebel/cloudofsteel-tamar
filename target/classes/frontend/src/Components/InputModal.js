import React from "react";

function InputModal(props){
    const setClass = () =>{
        let className = "form-control";
        if(props.isShow){
            className += "-plaintext";
        }
        return className;
    }

    return(
        <input type="text" className={setClass()} name={props.name} placeholder={props.placeholder} value={props.value} onChange={props.onChange} readOnly={props.isShow}/>
    )
}

export default InputModal;