import React, {useEffect, useState} from "react";
import "./App.css";
import InterceptorManagement from "./Components/InterceptorManagement";
import LoginScreen from "./Components/LoginScreen";
import MyProfile from "./Components/MyProfile";
import SupplyManagement from "./Components/SupplyManagement";
import LaunchData from "./Components/LaunchData";
import InterceptorData from "./Components/InterceptorData";
import ManagerOptions from "./Components/ManagerOptions";

const App = () => {
    const [loggedIn, setLoggedIn] = useState(false)
    const [isSpear, setIsSpear] = useState(false)
    const [screen, setScreen] = useState("InterceptorManagement")

    useEffect(() => {
        setLoggedIn(localStorage.getItem("user") !== "")
    }, [])
    const changeLoggedIn = (newBool) => {
        setLoggedIn(newBool)
    }
    // }
    const changeScreen = (newScreen) => {
        setScreen(newScreen)
    }
    const changeIsSpear = (newIsSpear) => {
        setIsSpear(newIsSpear)
        newIsSpear?changeScreen("InterceptorData"):changeScreen("InterceptorManagement")
    }
    const currScreen = () => {
        let element = ""
        switch (screen) {
            case "MyProfile":
                element = <MyProfile/>
                break;
            case "InterceptorManagement":
                element = <InterceptorManagement/>
                break;
            case "SupplyManagement":
                element = <SupplyManagement/>
                break;
            case "LaunchData":
                element = <LaunchData/>
                break;
            case "InterceptorData":
                element=<InterceptorData/>
                break
            case "ManagerOptions":
                element=<ManagerOptions/>
                break
        }
        return element
    }

    const logOut = (event) => {
        localStorage.clear();
        setLoggedIn(false);
    }
    const NavbarHot = ({changeScreen, changeIsSpear}) => {
        return <div className="row m-0">
            <nav dir="rtl" className="col-10 navbar justify-content-start navbar-light bg-primary">
                <div className="px-3"><h2>מחסן חם</h2></div>
                <a className="text-dark p-2" onClick={() => changeScreen("InterceptorManagement")}>ניהול סוללות</a>
                <a className="text-dark p-2" onClick={() => changeScreen("MyProfile")}>הפרופיל שלי</a>
                <a className="text-dark p-2" onClick={logOut}>התנתקות</a>
                <a className="text-dark p-2" onClick={() => changeScreen("SupplyManagement")}>ניהול מלאי</a>
                {localStorage.getItem("permission")=="1"?
                    <a className="text-dark p-2" onClick={() => changeScreen("ManagerOptions")}>אפשרויות מנהל</a>:""}


            </nav>
            <div className="col-2 bg-secondary align-middle" onClick={() => changeIsSpear(true)}>
                <h2 className="h-100">
                    <a className="align-middle">
                        חוד החנית
                    </a>
                </h2>
            </div>
        </div>
    }

    const NavbarSpear = ({changeScreen, changeIsSpear}) => {
        return <div className="row m-0">
            <nav dir="rtl" className="col-10 navbar justify-content-start navbar-light bg-secondary">
                <div className="px-3"><h2>חוד החנית</h2></div>
                <a className="text-dark p-2" onClick={() => changeScreen("InterceptorData")}>סוללות</a>
                <a className="text-dark p-2" onClick={() => changeScreen("LaunchData")}>שיגורים</a>
            </nav>
            <div className="col-2 bg-primary align-middle" onClick={()=>changeIsSpear(false)}>
                <h2 className="h-100">
                    <a className="align-middle">
                        מחסן חם
                    </a>
                </h2>
            </div>
        </div>
    }
    return (
        <div className="App" dir="rtl">
            <bdi>
                {!loggedIn
                    ? <LoginScreen logChangeFunc={changeLoggedIn}/>
                    : <div>{(isSpear ? <NavbarSpear changeScreen={changeScreen} changeIsSpear={changeIsSpear}/> :
                        <NavbarHot changeScreen={changeScreen} changeIsSpear={changeIsSpear}/>)}
                        <div>
                            {currScreen()}
                        </div>
                </div>
                }
            </bdi>
        </div>
    );

}

export default App;
